import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import baseReducer from './src/redux/reducers/basereducer';
import thunk from 'redux-thunk';
import RootNavigator from './src/navigator/_rootNavigator'

const store = createStore(baseReducer, applyMiddleware(thunk))

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <RootNavigator />
      </Provider>
    );
  }
}


