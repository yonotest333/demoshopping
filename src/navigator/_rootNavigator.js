import { createStackNavigator, createAppContainer } from 'react-navigation';
import ListScreen from '../screens/listPage';
import StoreDetails from '../screens/storeDetails';
import ProductDetails from '../screens/productDetails';
import SaveProducts from '../screens/saveProducts';

const AppNavigator = createStackNavigator({
  ListScreen: {
    screen: ListScreen,
  },
  StoreDetails: {
    screen: StoreDetails,
  },
  ProductDetails: {
    screen: ProductDetails,
  },
  SaveProducts: {
    screen: SaveProducts,
  },
});

export default createAppContainer(AppNavigator);
