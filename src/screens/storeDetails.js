/* eslint-disable no-console */
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
  AsyncStorage,
  Platform,
  Dimensions
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { storeIdDetails, productList } from '../common/api';
import * as CONSTANTS from '../common/constant';


let TAG = 'Store Details : --';

let _this = null;

class storeDetails extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerRight: <View style={styles.navigationView} >
      <TouchableOpacity onPress={() => _this.saveData()}>
        <Image
          style={styles.saveButtonView}
          source={{ uri: 'https://img.icons8.com/dotty/80/000000/save.png' }}
        />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate('SaveProducts')}>
        <Image
          style={styles.cartView}
          source={{ uri: 'https://img.icons8.com/dotty/80/000000/shopping-cart.png' }}
        />
      </TouchableOpacity>
    </View>,
    headerLeft: <TouchableOpacity onPress={() => navigation.goBack()} ><Image
      style={styles.backButton}
      source={{ uri: 'https://img.icons8.com/dotty/80/000000/arrow-pointing-left.png' }}
    />
    </TouchableOpacity>,
    headerStyle: styles.headerStyle,
    headerTitle: <View style={styles.titleView} ><Text style={{ fontWeight: 'bold', fontSize: 20 }} >Store Details</Text></View>
  });

  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      storeDetails: [],
      productListValues: [],
    };
  }

  componentWillMount() {
    this.props.storeIdDetails(this.props.navigation.state.params.storeId);
    this.props.productList(this.props.navigation.state.params.storeId);
  }

  componentWillReceiveProps(nextProps) {
    const { productListValues } = this.state;

    if (nextProps.baseReducer.type === CONSTANTS.STORE_RESPONSE_SUCCESS) {
      if (nextProps.baseReducer.response !== undefined) {
        this.setState({
          storeDetails: nextProps.baseReducer.response.store,
        });
      }
    }


    if (nextProps.baseReducer.type === CONSTANTS.PRODUCT_RESPONSE_SUCCESS) {
      if (nextProps.baseReducer.response !== undefined) {
        this.setState({
          productListValues: nextProps.baseReducer.response.products,
        }, () => {
          productListValues.map((item) => {
            item.isSave = false;
          });
        }
        );
      }
    }
  }

  itemSelection(item) {
    const { productListValues } = this.state;
    let selectedData = productListValues;
    let selectedItem = selectedData.find(x => x._id === item._id);
    let selectedIndex = selectedData.findIndex(x => x._id === item._id);

    if (selectedItem.isSave) {
      selectedItem.isSave = false;
    } else {
      selectedItem.isSave = true;
    }

    selectedData[selectedIndex] = selectedItem;

    this.setState({
      productListValues: selectedData,
    });
  }

  async saveData() {
    let saveItems = [];
    const { productListValues } = this.state;
    productListValues.map(item => {
      if (item.isSave === true) {
        saveItems.push(item);
      }
    });
    if (saveItems != undefined) {
      await AsyncStorage.setItem('SaveProducts', JSON.stringify(saveItems));
    }
  }

  renderItem(item) {
    return (
      <TouchableOpacity
        style={styles.flatview}
        onPress={() => this.props.navigation.navigate('ProductDetails', { productId: item._id })}
      >
        <Image
          style={{ height: 40, width: 40, marginRight: 7 }}
          source={{ uri: 'https://img.icons8.com/ios/50/000000/box.png' }}
        />
        <View style={{ flex: 1 }}>
          <Text style={styles.name}>{item.name}</Text>
          <Text style={styles.name}>{item.category}</Text>
        </View>
        <TouchableOpacity
          style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}
          onPress={() => this.itemSelection(item)}
        >
          <Image
            style={{ height: 20, width: 20 }}
            source={{
              uri: item.isSave
                ? 'https://img.icons8.com/ios/50/000000/checked-checkbox.png'
                : 'https://img.icons8.com/metro/26/000000/unchecked-checkbox.png',
            }}
          />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  }

  render() {
    let { storeDetails, productListValues } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.detailsView}>
          <Text style={styles.titleText}>Store Name:-</Text>
          <Text style={styles.detailsText}>{storeDetails.acnName}</Text>
        </View>
        <View style={styles.lowDetailsView}>
          <Text style={styles.titleText}>Store Address:-</Text>
          <Text style={styles.detailsText}>{storeDetails.fullAddress}</Text>
        </View>
        <View style={styles.lowDetailsView}>
          <Text style={styles.titleText}>Store Email:-</Text>
          <Text style={styles.detailsText}>{storeDetails.businessEmail}</Text>
        </View>
        <View style={styles.productListView}>
          <View style={styles.borderVIew}>
            <Text style={{ textAlign: 'center' }}>Product List</Text>
          </View>
          <FlatList
            data={productListValues}
            renderItem={({ item }) => this.renderItem(item)}
            keyExtractor={item => item._id}
            extraData={this.state}
          />
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  console.log(TAG, state);
  return {
    baseReducer: state
  };
}

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ storeIdDetails, productList }, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(storeDetails);

let styles = StyleSheet.create({
  mainHeaderView: {
    width: "100%",
    height: (Platform.OS == "android") ? 20 : (CONSTANTS.iPhoneX) ? 44 : 25,
    backgroundColor: 'transparent',
  },
  subHeaderView: {
    flexDirection: "row",
    width: Dimensions.get("window").width,
    // flex: 1,
    alignItems: "center",
  },
  headerStyle: {
    borderBottomWidth: 0,
    elevation: 0,
    shadowColor: "transparent",
    textAlign: "center",
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  navigationView: {
    flexDirection: 'row',
  },
  backButton: {
    height: 30,
    width: 30,
    marginLeft: 15,
  },
  titleView: {
    alignItems: "center",
    flex: 1
  },
  saveButtonView: {
    height: 30,
    width: 30,
    marginLeft: 70,
    marginRight: 15,
  },
  cartView: {
    height: 30,
    width: 30,
    marginRight: 15,
  },
  detailsView: {
    flexDirection: 'row',
    marginLeft: 15,
    marginTop: 20,
  },
  lowDetailsView: {
    flexDirection: 'row',
    marginLeft: 15,
    marginTop: 10,
  },
  titleText: {
    fontSize: 15,
  },
  detailsText: {
    fontSize: 15,
    marginLeft: 10,
  },
  productListView: {
    marginTop: 20,
    flex: 1,
  },
  borderVIew: {
    width: '100%',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    paddingVertical: 10,
  },
  flatview: {
    alignItems: 'center',
    paddingTop: 30,
    borderRadius: 2,
    flexDirection: 'row',
    marginHorizontal: 15,
    flex: 1,
  },
  name: {
    fontFamily: 'Verdana',
    fontSize: 18,
  },
  email: {
    color: 'red',
  },
});
