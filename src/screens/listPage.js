import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList, Image, TouchableOpacity, TextInput, Dimensions, Platform } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { listStore, searchStore } from '../common/api';
import Loader from '../component/loader';
import * as CONSTANTS from '../common/constant';

const TAG = 'ListScreen';

let copyOfData = [];

class ListPage extends Component {
  static navigationOptions = {
    headerTitle: 'Store List',
  };

  constructor(props) {
    super(props);
    this.state = {
      listData: [],
    };
  }

  componentDidMount() {
    const { listStore } = this.props;
    listStore();
  }

  componentWillReceiveProps(nextProps) {
    console.log("NextProps-->", nextProps);
    if (nextProps.baseReducer.type === CONSTANTS.RESPONSE_SUCCESS) {
      if (nextProps.baseReducer.response !== undefined) {
        this.setState({
          listData: nextProps.baseReducer.response.stores,
        }, () => {
          copyOfData = this.state.listData;
        }
        );
      }
    }
  }

  onSearchStore(text) {
    this.setState({ searchQuery: text }, () => {
      if (text.trim().length > 0) {
        const arrayholder = copyOfData;
        const newData = arrayholder.filter(item => {
          if (item.tradingName) {
            const itemData = item.tradingName.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
          }
        });
        this.setState({ listData: newData });
      } else {
        this.setState({ listData: copyOfData });
      }
    });
  }

  renderItem(item) {
    const { navigation } = this.props;

    return item.tradingName ? (
      <TouchableOpacity
        style={styles.flatview}
        onPress={() => navigation.navigate('StoreDetails', { storeId: item.storeId })}
      >
        <Image
          style={styles.imageStyle}
          source={{ uri: 'https://img.icons8.com/color/48/000000/small-business.png' }}
        />
        <Text numberOfLines={1} style={styles.name}>{item.tradingName.charAt(0).toUpperCase() + item.tradingName.slice(1)}</Text>
      </TouchableOpacity>
    ) : null;
  }

  render() {
    const { searchQuery, listData } = this.state;
    const { baseReducer } = this.props;
    return (
      <View style={{ flex: 1 }} >
        <TextInput
          value={searchQuery}
          placeholder="Search Here"
          autoCapitalize="none"
          onChangeText={text => this.onSearchStore(text)}
          style={styles.textInputStyle}
        />
        {baseReducer.isLoading ? (
          <Loader />
        ) : (
            <View style={styles.flatListView} >
              <FlatList
                style={styles.flatList}
                data={listData}
                renderItem={({ item }) => this.renderItem(item)}
                keyExtractor={item => item.storeId}
                keyboardShouldPersistTaps={'handled'}
                numColumns={3}
                showsVerticalScrollIndicator={false}
              />
            </View>
          )}
      </View>
    );
  }
}

function mapStateToProps(state) {
  console.log(TAG, state);
  return {
    baseReducer: state
  };
}

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ listStore, searchStore }, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListPage);

const styles = StyleSheet.create({
  flatListView: {
    flex: 1,
    alignItems: 'center'
  },
  flatList: {
    flex: 1,
    marginTop: 10
  },
  flatview: {
    alignItems: 'center',
    width: Dimensions.get('window').width * 0.302,
    justifyContent: 'center',
    padding: 10,
    margin: 5,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowColor: 'gray',
    shadowOpacity: 0.2,
    elevation: 2,
    backgroundColor: 'white'
  },
  name: {
    fontFamily: 'Verdana',
    fontSize: 12,
    textAlign: 'center',
    color: 'gray'
  },
  email: {
    color: 'red',
  },
  textInputStyle: {
    // marginHorizontal: 15,
    textAlign: 'center',
    marginTop: 30,
    width: '93%',
    height: 40,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 10,
    alignSelf: 'center',
  },
  imageStyle: {
    height: 50,
    width: 50,
  },
});
