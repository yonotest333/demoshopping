import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { productDetails } from '../common/api';
import * as CONSTANTS from '../common/constant';

const TAG = 'productDetails : --';

class ProductData extends Component {
  static navigationOptions = {
    headerTitle: 'Product Details',
  };

  constructor(props) {
    super(props);
    this.state = {
      productDetails: [],
    };
  }

  componentWillMount() {
    this.props.productDetails(this.props.navigation.state.params.productId);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.baseReducer.type === CONSTANTS.PRODUCT_DETAILS_RESPONSE_SUCCESS) {
      if (nextProps.baseReducer.response !== undefined) {
        this.setState({
          productDetails: nextProps.baseReducer.response.product,
        });
      }
    }
  }

  render() {
    const { productDetails } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.detailsView}>
          <Text style={styles.titleText}>Product Name:-</Text>
          <Text style={styles.detailsText}>{productDetails.name}</Text>
        </View>
        <View style={styles.lowDetailsView}>
          <Text style={styles.titleText}>Product Description:-</Text>
          <Text style={styles.detailsText}>{productDetails.description}</Text>
        </View>
        <View style={styles.lowDetailsView}>
          <Text style={styles.titleText}>Product Price:-</Text>
          <Text style={styles.detailsText}>{productDetails.priceBux}</Text>
        </View>

        <View style={styles.imageVIew}>
          <Image style={styles.imageStyle} source={{ uri: productDetails.imageUrl }} />
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  console.log(TAG, state);
  return {
    baseReducer: state
  };
}

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ productDetails }, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductData);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  imageVIew: {
    flex: 1,
    marginTop: 20,
    alignItems: 'center',
  },
  detailsView: {
    flexDirection: 'row',
    marginLeft: 15,
    marginTop: 20,
  },
  lowDetailsView: {
    flexDirection: 'row',
    marginLeft: 15,
    marginTop: 10,
  },
  titleText: {
    fontSize: 15,
  },
  detailsText: {
    fontSize: 15,
    marginLeft: 10,
  },
  imageStyle: {
    height: 200,
    width: 400,
  },
  flatview: {
    alignItems: 'center',
    paddingTop: 30,
    borderRadius: 2,
    flexDirection: 'row',
    marginHorizontal: 15,
    flex: 1,
  },
  name: {
    fontFamily: 'Verdana',
    fontSize: 18,
  },
  email: {
    color: 'red',
  },
});
