import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList, Image, AsyncStorage, TouchableOpacity } from 'react-native';

class saveProducts extends Component {
  static navigationOptions = {
    headerTitle: 'Saved Products',
  };

  constructor(props) {
    super(props);
    this.state = {
      savedProducts: [],
    };
  }

  async componentWillMount() {
    const savedvalue = await AsyncStorage.getItem('SaveProducts');
    console.log('Saved Value', savedvalue);

    if (savedvalue !== undefined && savedvalue !== null) {
      this.setState({
        savedProducts: JSON.parse(savedvalue),
      });
    }
  }

  deleteItem(item) {

    const selectedData = this.state.savedProducts;
    const selectedItem = selectedData.find(x => x._id === item._id);
    const selectedIndex = selectedData.findIndex(x => x._id === item._id);

    selectedItem.isSave = false;

    selectedData[selectedIndex] = selectedItem;

    selectedData.slice(selectedIndex, 1);

    this.setState({
      saveProducts: selectedData,
    }, () => {
      AsyncStorage.setItem('SaveProducts', JSON.stringify(this.state.saveProducts));
    }
    );
  }

  renderItem(item) {
    if (item.isSave) {
      return (
        <View style={styles.flatview}>
          <Image
            style={styles.imageView}
            source={{ uri: 'https://img.icons8.com/ios/50/000000/box.png' }}
          />
          <View style={{ flex: 1 }}>
            <Text style={styles.name}>{item.name}</Text>
            <Text style={styles.name}>{item.category}</Text>
          </View>
          <TouchableOpacity onPress={() => this.deleteItem(item)}>
            <Image
              style={{ height: 30, width: 30 }}
              source={{ uri: 'https://img.icons8.com/metro/26/000000/close-window.png' }}
            />
          </TouchableOpacity>
        </View>
      );
    }
    return null;
  }

  render() {
    const { savedProducts } = this.state;
    return (
      <View style={styles.container}>
        <FlatList
          data={savedProducts}
          renderItem={({ item }) => this.renderItem(item)}
          keyExtractor={item => item._id}
          extraData={this.state}
        />
      </View>
    );
  }
}

export default saveProducts;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  imageView: {
    height: 40,
    width: 40,
    marginRight: 7,
  },
  flatview: {
    alignItems: 'center',
    paddingTop: 30,
    borderRadius: 2,
    flexDirection: 'row',
    marginHorizontal: 15,
    flex: 1,
  },
  name: {
    fontFamily: 'Verdana',
    fontSize: 18,
  },
  email: {
    color: 'red',
  },
});
