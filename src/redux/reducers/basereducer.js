import * as constants from '../../common/constant';

const initialState = {
  response: undefined,
  isLoading: false,
  error: false,
};

export default function baseReducer(state = initialState, action) {
  console.log(`baseReducer : action -> ${JSON.stringify(action)}`);
  switch (action.type) {
    case constants.SEARCH_FETCH_DATA:
    case constants.STORE_FETCH_DATA:
    case constants.PRODUCT_FETCH_DATA:
    case constants.PRODUCT_DETAILS_FETCH_DATA:
    case constants.FETCH_DATA:
      return {
        ...state,
        isLoading: true,
        response: undefined,
        type: action.type,
      };
    case constants.SEARCH_RESPONSE_SUCCESS:
    case constants.STORE_RESPONSE_SUCCESS:
    case constants.PRODUCT_RESPONSE_SUCCESS:
    case constants.PRODUCT_DETAILS_RESPONSE_SUCCESS:
    case constants.RESPONSE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        response: action.json,
        type: action.type,
      };
    case constants.SEARCH_RESPONSE_ERROR:
    case constants.STORE_RESPONSE_ERROR:
    case constants.PRODUCT_RESPONSE_ERROR:
    case constants.PRODUCT_DETAILS_RESPONSE_ERROR:
    case constants.RESPONSE_ERROR:
      return {
        ...state,
        isLoading: false,
        error: true,
        type: action.type,
      };
    default:
      return state;
  }
}
