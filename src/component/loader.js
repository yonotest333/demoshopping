/* eslint-disable no-console */
import React, { Component } from 'react';
import { StyleSheet, View, ActivityIndicator, Dimensions } from 'react-native';

export default class Loader extends React.Component {
  componentWillReceiveProps(nextProps) {
    console.log(`componentWillReceiveProps : nextProps -> ${JSON.stringify(nextProps)}`);
  }

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator style={styles.animationStyle} size="large" color="#fff" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: Dimensions.get('screen').height,
    width: Dimensions.get('screen').width,
    backgroundColor: 'transparent',
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
  },
  animationStyle: {
    width: 100,
    height: 100,
    backgroundColor: 'rgba(0,0,0,0.5)',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: 'transparent',
  },
});
