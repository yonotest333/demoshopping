import { Dimensions, Platform } from "react-native";

// Actions Constant
export const FETCH_DATA = 'FETCH_DATA';
export const RESPONSE_SUCCESS = 'RESPONSE_SUCCESS';
export const RESPONSE_ERROR = 'RESPONSE_ERROR';

export const STORE_FETCH_DATA = 'STORE_FETCH_DATA';
export const STORE_RESPONSE_SUCCESS = 'STORE_RESPONSE_SUCCESS';
export const STORE_RESPONSE_ERROR = 'STORE_RESPONSE_ERROR';

export const PRODUCT_FETCH_DATA = 'PRODUCT_FETCH_DATA';
export const PRODUCT_RESPONSE_SUCCESS = 'PRODUCT_RESPONSE_SUCCESS';
export const PRODUCT_RESPONSE_ERROR = 'PRODUCT_RESPONSE_ERROR';

export const PRODUCT_DETAILS_FETCH_DATA = 'PRODUCT_DETAILS_FETCH_DATA';
export const PRODUCT_DETAILS_RESPONSE_SUCCESS = 'PRODUCT_DETAILS_RESPONSE_SUCCESS';
export const PRODUCT_DETAILS_RESPONSE_ERROR = 'PRODUCT_DETAILS_RESPONSE_ERROR';

export const SEARCH_FETCH_DATA = 'SEARCH_FETCH_DATA';
export const SEARCH_RESPONSE_SUCCESS = 'SEARCH_RESPONSE_SUCCESS';
export const SEARCH_RESPONSE_ERROR = 'SEARCH_RESPONSE_ERROR';

// API EndPoint
export const baseUrl = 'http://ubux.biz/test/';
export const listStore = 'get-all-stores';
export const storeDetails = 'get-store?storeId=';
export const productList = 'get-store-products?storeId=';
export const productDetails = 'get-product?productId=';
export const searchStore = 'test/search-store';

// Window Size
export const iPhoneX = (Platform.OS === "ios" && Dimensions.get("window").height === 812 && Dimensions.get("window").width === 375);
