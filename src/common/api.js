import * as Constant from './constant';

const TAG = 'API';

export function listStore() {
  console.log(TAG, 'listStore : ');
  return dispatch => {
    console.log(TAG, 'listStore ---> : ');
    dispatch({ type: Constant.FETCH_DATA });
    return fetch(Constant.baseUrl + Constant.listStore, {
      method: 'GET',
    })
      .then(res => res.json())
      .then(json => {
        console.log(TAG, JSON.stringify(json));
        return dispatch({ type: Constant.RESPONSE_SUCCESS, json });
      })
      .catch(err => dispatch({ type: Constant.RESPONSE_ERROR }));
  };
}

export function storeIdDetails(storeId) {
  console.log(TAG, 'storeDetails : ');
  return dispatch => {
    console.log(TAG, 'storeDetails ---> : ');
    dispatch({ type: Constant.STORE_FETCH_DATA });
    return fetch(Constant.baseUrl + Constant.storeDetails + storeId, {
      method: 'GET',
    })
      .then(res => res.json())
      .then(json => {
        console.log(TAG, JSON.stringify(json));
        return dispatch({ type: Constant.STORE_RESPONSE_SUCCESS, json });
      })
      .catch(err => dispatch({ type: Constant.STORE_RESPONSE_ERROR }));
  };
}

export function productList(storeId) {
  console.log(TAG, 'productList : ');
  return dispatch => {
    console.log(TAG, 'productList ---> : ');
    dispatch({ type: Constant.PRODUCT_FETCH_DATA });
    return fetch(Constant.baseUrl + Constant.productList + storeId, {
      method: 'GET',
    })
      .then(res => res.json())
      .then(json => {
        console.log(TAG, JSON.stringify(json));
        return dispatch({ type: Constant.PRODUCT_RESPONSE_SUCCESS, json });
      })
      .catch(err => dispatch({ type: Constant.PRODUCT_RESPONSE_ERROR }));
  };
}

export function productDetails(productId) {
  console.log(TAG, 'productDetails : ');
  return dispatch => {
    console.log(TAG, 'productDetails ---> : ');
    dispatch({ type: Constant.PRODUCT_DETAILS_FETCH_DATA });
    return fetch(Constant.baseUrl + Constant.productDetails + productId, {
      method: 'GET',
    })
      .then(res => res.json())
      .then(json => {
        console.log(TAG, JSON.stringify(json));
        return dispatch({ type: Constant.PRODUCT_DETAILS_RESPONSE_SUCCESS, json });
      })
      .catch(err => dispatch({ type: Constant.PRODUCT_DETAILS_RESPONSE_ERROR }));
  };
}

export function searchStore(searchText) {
  console.log(TAG, 'searchStores : ');
  return dispatch => {
    console.log(TAG, 'searchStore ---> : ');
    dispatch({ type: Constant.SEARCH_FETCH_DATA });
    return fetch(Constant.baseUrl + Constant.searchStore + searchText, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(json => {
        console.log(TAG, JSON.stringify(json));
        return dispatch({ type: Constant.SEARCH_RESPONSE_SUCCESS, json });
      })
      .catch(err => dispatch({ type: Constant.SEARCH_RESPONSE_ERROR }));
  };
}
